/*const { Pool } = require('pg')

const pool = new Pool({
  user: 'proj1db_user',//process.env.DB_USER,
  host: 'postgres://proj1db_user:5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo@dpg-cdfcuj5a4992md4e4kh0-a.oregon-postgres.render.com/proj1db',//process.env.DB_HOST,
  database: 'proj1db',
  password: '5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo',//process.env.DB_PASSWORD,
  port: 5432,
  //ssl : true
})

let querryARR = []

const query1 = {
  text: 'DROP TABLE IF EXISTS proj2table',
  values: [],
  rowMode: 'array'
}
querryARR[0] = query1

const query2 = {
  text: 'CREATE TABLE proj2table (id int, textvar varchar(100))',
  values: [],
  rowMode: 'array'
}
querryARR[1] = query2

const query3 = {
  text: 'INSERT INTO proj2table VALUES (1, $1)',
  values: ['John Smith'],
  rowMode: 'array'
}
querryARR[2] = query3

const query4 = {
  text: 'INSERT INTO proj2table VALUES (2, $1)',
  values: ['Elizabeth II'],
  rowMode: 'array'
}
querryARR[3] = query4

const query5 = {
  text: 'INSERT INTO proj2table VALUES (3, $1)',
  values: ['Carmen'],
  rowMode: 'array'
}
querryARR[4] = query5

const query6 = {
  text: 'INSERT INTO proj2table VALUES (4, $1)',
  values: ['Mark'],
  rowMode: 'array'
}
querryARR[5] = query6

const query7 = {
  text: 'INSERT INTO proj2table VALUES (5, $1)',
  values: ['Quincy'],
  rowMode: 'array'
}
querryARR[6] = query7

const query8 = {
  text: 'INSERT INTO proj2table VALUES (6, $1)',
  values: ['Nero'],
  rowMode: 'array'
}
querryARR[7] = query8

const query9 = {
  text: 'INSERT INTO proj2table VALUES (7, $1)',
  values: ['House'],
  rowMode: 'array'
}
querryARR[8] = query9

const query10 = {
  text: 'INSERT INTO proj2table VALUES (8, $1)',
  values: ['Julius'],
  rowMode: 'array'
}
querryARR[9] = query10

const query11 = {
  text: 'INSERT INTO proj2table VALUES (9, $1)',
  values: ['Augustus'],
  rowMode: 'array'
}
querryARR[10] = query11

const query12 = {
  text: 'INSERT INTO proj2table VALUES (10, $1)',
  values: ['Johny'],
  rowMode: 'array'
}
querryARR[11] = query12

const query13 = {
  text: 'INSERT INTO proj2table VALUES (42, $1)',
  values: ['PASSWORD4321'],
  rowMode: 'array'
}
querryARR[12] = query13;

(async () => {
try {
  console.log('TASK START')
  for(let q in querryARR){
    console.log(q)
    //console.log(q.values)
    //const res = await pool.query(q.text, q.values)
    await pool.query(q.text, q.values)
  }
  console.log('TASK DONE')
} catch (err) {
  console.log(err.stack)
}
})();*/

import dotenv from 'dotenv'
import { Pool } from 'pg'
dotenv.config()

const connectionString = 'postgres://proj1db_user:5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo@dpg-cdfcuj5a4992md4e4kh0-a.oregon-postgres.render.com/proj1db'

const pool = new Pool({
  //user: 'proj1db_user',//process.env.DB_USER,
  //host: 'dpg-cdfcuj5a4992md4e4kh0-a',//process.env.DB_HOST,
  //database: 'proj1db',//process.env.DB_NAME,
  //password: '5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo',//String(process.env.DB_PASSWORD),
  //port: 5432,
  ssl : true,
  connectionString,
})

let querryARR = []

const query1 = {
  text: 'DROP TABLE IF EXISTS proj2table',
  values: [],
  rowMode: 'array',
}
querryARR.push(query1)

const query2 = {
  text: 'CREATE TABLE proj2table (id int, textvar varchar(100))',
  values: [],
  rowMode: 'array',
}
querryARR.push(query2)

const query3 = {
  text: 'INSERT INTO proj2table VALUES (1, $1)',
  values: ['John Smith'],
  rowMode: 'array',
}
querryARR.push(query3)

const query4 = {
  text: 'INSERT INTO proj2table VALUES (2, $1)',
  values: ['Elizabeth II'],
  rowMode: 'array',
}
querryARR.push(query4)

const query5 = {
  text: 'INSERT INTO proj2table VALUES (3, $1)',
  values: ['Carmen'],
  rowMode: 'array',
}
querryARR.push(query5)

const query6 = {
  text: 'INSERT INTO proj2table VALUES (4, $1)',
  values: ['Mark'],
  rowMode: 'array',
}
querryARR.push(query6)

const query7 = {
  text: 'INSERT INTO proj2table VALUES (5, $1)',
  values: ['Quincy'],
  rowMode: 'array',
}
querryARR.push(query7)

const query8 = {
  text: 'INSERT INTO proj2table VALUES (6, $1)',
  values: ['Nero'],
  rowMode: 'array',
}
querryARR.push(query8)

const query9 = {
  text: 'INSERT INTO proj2table VALUES (7, $1)',
  values: ['House'],
  rowMode: 'array',
}
querryARR.push(query9)

const query10 = {
  text: 'INSERT INTO proj2table VALUES (8, $1)',
  values: ['Julius'],
  rowMode: 'array',
}
querryARR.push(query10)

const query11 = {
  text: 'INSERT INTO proj2table VALUES (9, $1)',
  values: ['Augustus'],
  rowMode: 'array',
}
querryARR.push(query11)

const query12 = {
  text: 'INSERT INTO proj2table VALUES (10, $1)',
  values: ['Johny'],
  rowMode: 'array',
}
querryARR.push(query12)

const query13 = {
  text: 'INSERT INTO proj2table VALUES (42, $1)',
  values: ['PASSWORD4321'],
  rowMode: 'array',
}
querryARR.push(query13);

//querryARR.push(55)
//console.log(query1.text);

(async () => {
try {
  console.log('TASK START')
  await pool.query(query2)
  //console.log(querryARR.length)
  for(let i in querryARR){
    console.log(i)
    //console.log(q.values)
    //const res = await pool.query(q.text, q.values)
    await pool.query(querryARR[i].text, querryARR[i].values)
  }
  console.log('TASK DONE')
} catch (err) {
  console.log(err.stack)
}
})();