import express from 'express';
import fs from 'fs';
import path from 'path'
import https from 'https';
import { auth, requiresAuth } from 'express-openid-connect'; 
import dotenv from 'dotenv'
import { Pool } from 'pg'
dotenv.config()

const app = express();
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');

//const port = 4080;
const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

//let admins = ["ADMIN"]

const config = { 
  authRequired : false,
  idpLogout : true, //login not only from the app, but also from identity provider
  secret: process.env.SECRET,
  baseURL: externalUrl || `https://localhost:${port}`,
  clientID: 'GdohVAAmkHY7Qfg9g0sH1SkTHWIh95tp',
  issuerBaseURL: 'https://dev-c7pibxseu5z2fg0n.us.auth0.com',
  clientSecret: 'lt0V2OzMEs-oQg5NUaCGtD7IM0KkjuXsbOk4kz5dXVnGBFDV_ipXZr6KuXybI1dA',
  authorizationParams: {
    response_type: 'code' ,
    //scope: "openid profile email"   
   },
};
// auth router attaches /login, /logout, and /callback routes to the baseURL
//app.use(auth(config));

app.get('/',  function (req, res) {
  res.sendFile(__dirname + '/views/proj3.html');
});

/*https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
    console.log(`Server running at https://localhost:${port}/`);
  });*/

  if (externalUrl) {
    const hostname = '127.0.0.1';
    app.listen(port, hostname, () => {
    console.log(`Server locally running at http://${hostname}:${port}/ and from 
    outside on ${externalUrl}`);
    });
    }
    else {
    https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function () {
    console.log(`Server running at https://localhost:${port}/`);
    });
    }
